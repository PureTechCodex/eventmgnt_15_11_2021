-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2019 at 02:08 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `event`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `about_us_id` int(10) UNSIGNED NOT NULL,
  `about_us_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`about_us_id`, `about_us_text`, `created_at`, `updated_at`) VALUES
(1, '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like) It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English</p>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like) It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English</p>', '2019-01-11 10:44:26', '2019-01-14 15:40:03');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_firstname`, `admin_lastname`, `email`, `password`, `remember_token`, `admin_image`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'user', 'admin@event.com', '$2y$10$slNSiqaxGMw0V.WzWRGUQe4ilW/MX4CmKS4lotbrIc9B9HwhVj6hq', 'BJozzf6W02V9XJBLoXCAhqlBdzJkOXMaDlPe5IvJysdzKwxDAiknobzpbaBZ', 'uploads/admin/admin.jpg', NULL, '2018-12-18 08:04:57');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `event_id` int(10) UNSIGNED NOT NULL,
  `event_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `main_event_id` int(11) NOT NULL,
  `event_category_id` int(10) UNSIGNED NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_user_id` int(11) DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gallery_path` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `number_of_attendees` int(7) DEFAULT '0',
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`event_id`, `event_name`, `event_image`, `event_detail`, `start_date`, `end_date`, `main_event_id`, `event_category_id`, `country`, `event_status`, `event_user_id`, `city`, `gallery_path`, `created_by`, `updated_by`, `number_of_attendees`, `state`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Test Event', 'uploads/events/Test Event//tmp/phpOAp4aK1547197210.jpg', 'as sa sa', '2018-12-20', '2018-12-31', 0, 1, 'India', 'approved', 11, 'test', NULL, 12, NULL, 0, 'Maharashtra', 'test test test test as asas', '2018-12-20 12:52:48', '2019-02-01 11:40:32'),
(2, 'Test Event', 'uploads/events/Test Event//tmp/phpemssr21547197217.jpg', 'as sa sa', '2018-12-20', '2018-12-31', 0, 1, 'India', 'approved', NULL, 'test', NULL, 11, NULL, 0, 'Maharashtra', 'test test test test', '2018-12-20 12:53:04', '2019-02-01 11:40:36'),
(3, 'prov1', 'uploads/events/prov1.jpg', 'badminton', '2019-02-21', '2019-02-24', 0, 2, 'italia', 'approved', NULL, 'milano', NULL, NULL, NULL, 0, 'italy', 'palabadminton via g. ciambue 24', '2019-01-14 18:43:30', '2019-02-01 11:40:47'),
(4, 'italian junior 2019', 'uploads/events/italian junior 2019.', 'badminton europe circuit', '2019-02-21', '2019-02-24', 0, 2, 'Italia', 'approved', 11, 'milano', NULL, NULL, NULL, 0, 'open', 'stefanoinfantino@badmintonitalia.it', '2019-01-17 17:12:09', '2019-02-01 11:23:49');

-- --------------------------------------------------------

--
-- Table structure for table `event_booking`
--

CREATE TABLE `event_booking` (
  `event_booking_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_category`
--

CREATE TABLE `event_category` (
  `event_category_id` int(10) UNSIGNED NOT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  `event_category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_category`
--

INSERT INTO `event_category` (`event_category_id`, `parent_category_id`, `event_category_name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Music', NULL, NULL, '2018-12-20 12:00:53', '2018-12-20 12:02:23'),
(2, NULL, 'Sports', NULL, NULL, '2018-12-21 06:21:14', '2018-12-21 06:21:14');

-- --------------------------------------------------------

--
-- Table structure for table `event_ticket_assign`
--

CREATE TABLE `event_ticket_assign` (
  `event_ticket_assign_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `total_tickets` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `event_ticket_types`
--

CREATE TABLE `event_ticket_types` (
  `event_ticket_types_id` int(10) UNSIGNED NOT NULL,
  `event_ticket_types_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_ticket_types`
--

INSERT INTO `event_ticket_types` (`event_ticket_types_id`, `event_ticket_types_name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Gold', NULL, NULL, '2018-12-21 06:21:25', '2018-12-21 06:21:25');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `faq_id` int(10) UNSIGNED NOT NULL,
  `faq_question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `faq_answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`faq_id`, `faq_question`, `faq_answer`, `created_at`, `updated_at`) VALUES
(1, 'test faq 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining', '2018-11-30 01:28:04', '2019-01-11 11:53:03'),
(3, 'test faq 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining', '2018-11-30 01:37:36', '2019-01-11 11:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `hotel_id` int(10) UNSIGNED NOT NULL,
  `hotel_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_url` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotel_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map_address` text COLLATE utf8mb4_unicode_ci,
  `gallery_path` text COLLATE utf8mb4_unicode_ci,
  `hotel_contact_no1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_contact_no2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distance_to_airport` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distance_to_station` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distance_to_compassion` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review` text COLLATE utf8mb4_unicode_ci,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `hotel_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotel_rating` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`hotel_id`, `hotel_name`, `hotel_image`, `hotel_city`, `hotel_state`, `hotel_country`, `hotel_address`, `hotel_url`, `hotel_description`, `map_address`, `gallery_path`, `hotel_contact_no1`, `hotel_contact_no2`, `distance_to_airport`, `distance_to_station`, `distance_to_compassion`, `review`, `created_by`, `updated_by`, `hotel_status`, `hotel_rating`, `created_at`, `updated_at`) VALUES
(1, 'test hotel', 'uploads/events/test hotel.jpg', 'test', 'Maharashtra', 'India', 'test test test test', 'https://www.hyatt.com/', 'asc sa', NULL, NULL, '232323', '2323', '30km', '20km', '20km', NULL, NULL, NULL, NULL, 3, '2018-12-24 05:45:33', '2019-01-24 06:41:56'),
(2, 'asc', '', 'test', 'test', 'test', 'testtesttesttest', NULL, 'test', NULL, NULL, '323323', '23232323', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL),
(3, 'asc', 'uploads/events/asc.jpg', 'test test test', 'Maharashtra', 'India', 'asc', 'https://www.hyatt.com/', 'asc', 'asc', NULL, '232323', '2323', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, '2019-01-23 13:03:51', '2019-01-23 13:03:51'),
(4, 'Orchid', 'uploads/events/Orchid.jpg', 'test test test', 'Maharashtra', 'India', 'as ddsvd', 'https://www.hyatt.com/', 'bh j j j j', 'www.google.com', NULL, '232323', '2323', '3km', '20km', '20km', 'as saa sa sa', NULL, NULL, NULL, 5, '2019-01-24 06:35:45', '2019-01-24 06:46:46');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_booking`
--

CREATE TABLE `hotel_booking` (
  `hotel_booking_id` int(10) UNSIGNED NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `room_type_id` int(11) NOT NULL,
  `check_in_date` date NOT NULL,
  `check_out_date` date NOT NULL,
  `pickup_datetime` datetime NOT NULL,
  `dropup_datetime` datetime NOT NULL,
  `amount` double NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_room`
--

CREATE TABLE `hotel_room` (
  `hotel_room_assign_id` int(10) UNSIGNED NOT NULL,
  `hotel_id` int(10) UNSIGNED NOT NULL,
  `hotel_room_type_id` int(10) UNSIGNED NOT NULL,
  `maximum_occupancy` int(11) NOT NULL,
  `discounted_price` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `conditions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_room`
--

INSERT INTO `hotel_room` (`hotel_room_assign_id`, `hotel_id`, `hotel_room_type_id`, `maximum_occupancy`, `discounted_price`, `conditions`, `price`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 30, '11', 'asc asc sa', 3000, NULL, NULL, '2018-12-25 08:33:16', '2019-01-24 07:11:58'),
(5, 1, 1, 30, '122', 'asc asc sa', 3000, NULL, NULL, '2019-01-24 07:03:04', '2019-01-24 07:13:39'),
(10, 1, 1, 30, '11', 'asc asc sa', 3000, NULL, NULL, '2019-01-24 07:09:52', '2019-01-24 07:09:52');

-- --------------------------------------------------------

--
-- Table structure for table `hotel_room_type`
--

CREATE TABLE `hotel_room_type` (
  `hotel_room_type_id` int(10) UNSIGNED NOT NULL,
  `hotel_room_type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_room_type`
--

INSERT INTO `hotel_room_type` (`hotel_room_type_id`, `hotel_room_type_name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Regular', NULL, NULL, '2018-12-24 08:09:22', '2018-12-24 09:18:24'),
(2, 'Normal', NULL, NULL, '2018-12-25 08:38:05', '2018-12-25 08:38:05');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(10) UNSIGNED NOT NULL,
  `eng_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ln_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ln_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `eng_text`, `ln_code`, `ln_text`, `created_at`, `updated_at`) VALUES
(1, 'Helloss', 'en', 'Ciao', '2018-12-25 10:46:12', '2018-12-25 10:48:30'),
(2, 'Hello', 'it', 'Ciao', '2018-12-25 10:47:01', '2018-12-25 10:47:01'),
(3, 'hi', 'en', 'hi', '2018-12-25 10:50:49', '2018-12-25 10:50:49'),
(4, 'aaaaa', 'it', 'bbbb', '2019-01-11 13:07:42', '2019-01-11 13:07:42');

-- --------------------------------------------------------

--
-- Table structure for table `meal`
--

CREATE TABLE `meal` (
  `meal_id` int(10) UNSIGNED NOT NULL,
  `meal_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meal_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meal_menu` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meal_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hotel_id` int(10) UNSIGNED NOT NULL,
  `meal_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meal`
--

INSERT INTO `meal` (`meal_id`, `meal_name`, `meal_type`, `meal_menu`, `meal_price`, `hotel_id`, `meal_status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Test meals', 'veg', 'menu 1\r\nmenu 2\r\nmenu 3', '1000', 2, NULL, NULL, NULL, '2018-12-26 10:55:41', '2018-12-26 12:12:44'),
(2, 'test 2', 'test', 'test\r\ntest', '87', 1, NULL, NULL, NULL, '2018-12-26 11:43:39', '2018-12-26 11:43:39'),
(3, 'test 2', 'test', 'test\r\ntest \r\ntest3', '87', 2, NULL, NULL, NULL, '2018-12-26 12:04:56', '2018-12-26 12:04:56'),
(4, 'test 2', 'test', 'test,test', '87', 1, NULL, NULL, NULL, '2018-12-26 12:11:03', '2018-12-26 12:11:03'),
(5, 'test 2', 'test s', 'test\r\ntest \r\ntest3', '87', 1, NULL, NULL, NULL, '2018-12-26 12:11:11', '2018-12-26 12:11:11'),
(6, 'Test meal', 'veg', 'menu 1\r\nmenu 2\r\nmenu 3', '1000', 1, NULL, NULL, NULL, '2018-12-26 12:11:37', '2018-12-26 12:11:37'),
(7, 'Test', 'test', 'test test', '9878', 1, NULL, NULL, NULL, '2018-12-27 04:49:01', '2018-12-27 04:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `meal_type`
--

CREATE TABLE `meal_type` (
  `meal_type_id` int(10) UNSIGNED NOT NULL,
  `meal_type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_22_093629_create_admin_table', 1),
(4, '2018_11_29_062057_create_permission_tables', 2),
(5, '2018_11_30_060910_create_faq_table', 3),
(6, '2018_11_30_092933_create_news_table', 4),
(7, '2018_11_30_103222_create_testimonial_table', 5),
(8, '2018_12_06_063018_create_news_category_table', 6),
(9, '2018_10_06_063019_create_news_category_table', 7),
(10, '2018_11_30_092934_create_news_table', 7),
(11, '2018_11_30_103223_create_testimonial_table', 7),
(12, '2014_12_12_000000_create_users_table', 8),
(13, '2014_12_12_0000100_create_users_table', 9),
(14, '2018_12_13_025230_create_event_category_table', 10),
(15, '2018_12_13_094551_create_event_table', 10),
(16, '2018_12_20_094551_create_event_table', 11),
(17, '2018_12_20_094560_create_event_table', 12),
(18, '2018_12_21_113707_create_event_ticket_types', 13),
(19, '2018_12_21_120731_create_event_ticket_assign_table', 14),
(20, '2018_12_22_234933_create_hotel_master_table', 15),
(21, '2018_12_24_122910_create_hotel_room_type_table', 16),
(22, '2018_12_24_145809_create_hotel_room_assign_table', 17),
(23, '2018_12_24_145811_create_hotel_room_assign_table', 18),
(24, '2018_12_24_145812_create_hotel_room_assign_table', 19),
(25, '2018_12_25_141701_create_transportation_table', 20),
(26, '2018_12_25_154331_create_language_table', 21),
(27, '2018_12_25_154332_create_language_table', 22),
(28, '2018_12_26_140938_create_meals_table', 23),
(29, '2018_12_26_140939_create_meals_table', 24),
(30, '2018_12_26_140940_create_meals_table', 25),
(31, '2018_12_26_162732_create_meal_type_table', 26),
(32, '2018_12_27_110040_create_package_table', 27),
(33, '2018_12_27_110734_create_package_details_table', 27),
(34, '2018_12_27_110735_create_package_details_table', 28),
(35, '2018_12_27_110044_create_package_table', 29),
(36, '2018_12_27_110738_create_package_details_table', 29),
(37, '2018_12_27_110739_create_package_details_table', 30),
(38, '2018_12_27_110045_create_package_table', 31),
(39, '2018_12_27_110740_create_package_details_table', 31),
(40, '2019_01_09_163640_create_manage_media_table', 32),
(41, '2019_01_09_163641_create_manage_media_table', 33),
(42, '2019_01_09_182408_create_placeholder_table', 34),
(43, '2019_02_01_173807_create_event_booking', 35),
(44, '2019_02_01_174032_create_hotel_booking', 35),
(45, '2019_02_01_174307_create_package_booking', 35),
(46, '2019_02_01_174744_create_payment_table', 35),
(47, '2019_02_01_180648_create_notification_table', 36);

-- --------------------------------------------------------

--
-- Table structure for table `mm_res`
--

CREATE TABLE `mm_res` (
  `mm_res_id` int(10) UNSIGNED NOT NULL,
  `media_path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `placeholder` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_media_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mm_res`
--

INSERT INTO `mm_res` (`mm_res_id`, `media_path`, `placeholder`, `max_media_size`, `media_type`, `created_at`, `updated_at`) VALUES
(6, 'uploads/media/home_page_slider1547125893_2019-01-10.jpg', 'home_page_slider', '536497', 'jpg', '2019-01-10 12:47:03', '2019-01-10 18:41:33'),
(7, 'uploads/media/about_us_heading1547112873_2019-01-10.jpg', 'about_us_heading', '504766', 'jpg', '2019-01-10 14:59:33', '2019-01-10 15:04:33'),
(9, 'uploads/media/news_heading1547113110_2019-01-10.jpg', 'news_heading', '195988', 'jpg', '2019-01-10 15:00:23', '2019-01-10 15:08:30'),
(10, 'uploads/media/contact_heading1547113182_2019-01-10.jpg', 'contact_heading', '200866', 'jpg', '2019-01-10 15:09:42', '2019-01-10 15:09:42'),
(11, 'uploads/media/event_heading1547113239_2019-01-10.jpg', 'event_heading', '360573', 'jpg', '2019-01-10 15:10:39', '2019-01-10 15:10:39'),
(12, 'uploads/media/home_page_slider1547189070_2019-01-11.jpg', 'home_page_slider', '157068', 'jpg', '2019-01-11 11:51:33', '2019-01-11 12:14:30'),
(13, 'uploads/events/Orchid.jpg', '', '', 'hotel', '2019-01-24 06:36:17', '2019-01-24 06:36:17'),
(14, 'uploads/events/Orchid.jpg', '', '', 'hotel', '2019-01-24 06:36:17', '2019-01-24 06:36:17'),
(15, 'uploads/events/Orchid.jpg', '', '', 'hotel', '2019-01-24 06:36:17', '2019-01-24 06:36:17');

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(1, 'App\\Models\\User', 2),
(1, 'App\\Models\\User', 9),
(1, 'App\\Models\\User', 12),
(2, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 3),
(2, 'App\\Models\\User', 4),
(2, 'App\\Models\\User', 5),
(2, 'App\\Models\\User', 6),
(2, 'App\\Models\\User', 7),
(2, 'App\\Models\\User', 8),
(2, 'App\\Models\\User', 10),
(2, 'App\\Models\\User', 11);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(10) UNSIGNED NOT NULL,
  `news_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `news_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `news_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `news_title`, `location`, `category_id`, `news_image`, `news_description`, `created_at`, `updated_at`) VALUES
(1, 'Andrea Pirlo Announces retirement', 'Italy', 1, 'uploads/news/Andrea Pirlo Announces retirement_2019-01-11.jpg', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2018-12-06 01:53:22', '2019-01-11 11:45:34'),
(2, 'Latest updates about CR7', 'Italy', 1, 'uploads/news/Latest updates about CR7_2019-01-11.jpg', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', '2019-01-11 11:46:18', '2019-01-11 11:46:18');

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE `news_category` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_category`
--

INSERT INTO `news_category` (`category_id`, `category_title`, `created_at`, `updated_at`) VALUES
(1, 'Cricket News', '2018-12-06 01:52:42', '2018-12-06 01:52:42');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `notification_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_done` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `package_id` int(10) UNSIGNED NOT NULL,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_description` text COLLATE utf8mb4_unicode_ci,
  `price` double NOT NULL,
  `package_status` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`package_id`, `package_name`, `package_image`, `package_description`, `price`, `package_status`, `created_by`, `updated_by`, `event_id`, `created_at`, `updated_at`) VALUES
(1, 'test', 'uploads/packages/test.jpg', 'as as', 3000, NULL, NULL, NULL, 1, '2018-12-28 06:42:51', '2019-01-02 07:08:39'),
(2, 'test 2', 'uploads/packages/test 2.jpg', NULL, 10000, NULL, NULL, NULL, 1, '2019-01-02 07:21:14', '2019-01-02 07:21:14'),
(3, 'Test package 2', 'uploads/packages/Test package 2.jpg', 'as sasa sa', 100, NULL, NULL, NULL, 1, '2019-01-02 09:07:02', '2019-01-02 09:07:02'),
(4, 'Package One', 'uploads/packages/Package One.png', 'ewewewewew', 45400, NULL, NULL, NULL, NULL, '2019-01-14 17:46:26', '2019-01-14 17:46:26');

-- --------------------------------------------------------

--
-- Table structure for table `package_booking`
--

CREATE TABLE `package_booking` (
  `package_booking_id` int(10) UNSIGNED NOT NULL,
  `package_id` int(11) NOT NULL,
  `package_details_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `package_details`
--

CREATE TABLE `package_details` (
  `package_details_id` int(10) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `meal_id` int(11) DEFAULT NULL,
  `room_type_id` int(11) DEFAULT NULL,
  `transportation_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package_details`
--

INSERT INTO `package_details` (`package_details_id`, `package_id`, `event_id`, `hotel_id`, `meal_id`, `room_type_id`, `transportation_id`, `created_at`, `updated_at`) VALUES
(5, 1, 1, 2, 1, 1, 2, '2019-01-02 11:40:42', '2019-01-02 11:40:42'),
(6, 3, 2, 2, 1, 2, 2, '2019-01-02 11:41:11', '2019-01-02 11:41:11'),
(7, 2, 1, 2, 1, 1, 2, '2019-01-02 11:41:21', '2019-01-02 11:41:21'),
(8, 3, 1, 1, 2, 3, 1, '2019-01-02 12:01:15', '2019-01-02 12:01:15'),
(9, 3, 1, 2, 1, 1, 2, '2019-01-03 08:36:15', '2019-01-03 08:36:15'),
(10, 3, 1, 1, NULL, NULL, 1, '2019-01-03 08:37:55', '2019-01-03 08:37:55');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('pranav.bitware@gmail.com', '$2y$10$GRA5vTuEf6uLdHHnWjULKO3rBSwfgAasZxA6vnk/w2E3s9CB26fi.', '2018-12-11 00:58:20'),
('shamjadhav73@gmail.com', '$2y$10$62ogN.oyW/5Gp4HeS8cimuyPvDPuQXite.21Y2a5ez/HVBFDOczyO', '2018-12-17 10:38:33');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `paypal` tinyint(1) NOT NULL,
  `credit_card` tinyint(1) NOT NULL,
  `credit_card_name_of_card` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry_month_year` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cvv_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pay_to_organizer` tinyint(1) NOT NULL,
  `amount` double NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `placeholder`
--

CREATE TABLE `placeholder` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'btoc', 'web', '2018-11-28 18:30:00', '2018-11-28 18:30:00'),
(2, 'btob', 'web', '2018-11-29 18:30:00', '2018-11-29 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `testimonials_id` int(10) UNSIGNED NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonial_review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`testimonials_id`, `location`, `user_name`, `user_image`, `testimonial_review`, `created_at`, `updated_at`) VALUES
(1, 'Pune', 'Test 1', 'uploads/testimonial/test 1_2018-12-06.jpg', 'Test review', '2018-12-06 03:40:01', '2018-12-06 03:40:01');

-- --------------------------------------------------------

--
-- Table structure for table `transportation`
--

CREATE TABLE `transportation` (
  `transportation_id` int(10) UNSIGNED NOT NULL,
  `vechicle_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vechicle_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `vechicle_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vechicle_state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vechicle_country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vechicle_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transportation_status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_seats` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transportation`
--

INSERT INTO `transportation` (`transportation_id`, `vechicle_type`, `vechicle_image`, `vechicle_city`, `vechicle_state`, `vechicle_country`, `vechicle_name`, `transportation_status`, `no_of_seats`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Bus', 'uploads/vechicle/Volvo b9r.jpg', 'test', 'Maharashtra', 'test', 'Volvo b9r', NULL, '48', NULL, NULL, '2018-12-26 06:23:30', '2018-12-26 07:16:45'),
(2, 'Bus', 'uploads/vechicle/Volvo b9r.jpg', 'test', 'Maharashtra', 'test', 'Volvo b9r', NULL, '45', NULL, NULL, '2018-12-26 06:23:59', '2018-12-26 06:23:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_firstname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_lastname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_image` text COLLATE utf8mb4_unicode_ci,
  `user_address` text COLLATE utf8mb4_unicode_ci,
  `user_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `user_gender` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org_service` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `user_email_verifyToken` text COLLATE utf8mb4_unicode_ci,
  `user_email_verified` int(2) NOT NULL DEFAULT '0',
  `any_special_requests` text COLLATE utf8mb4_unicode_ci,
  `user_forgot_password_token` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_password_updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `user_class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_firstname`, `user_lastname`, `user_country`, `user_image`, `user_address`, `user_type`, `user_username`, `email`, `contact_no`, `org_name`, `dob`, `user_gender`, `org_service`, `email_verified_at`, `user_email_verifyToken`, `user_email_verified`, `any_special_requests`, `user_forgot_password_token`, `password`, `last_password_updated_at`, `remember_token`, `last_login`, `user_class`, `online`, `created_at`, `updated_at`) VALUES
(11, 'test', 'asc', 'En', 'uploads/users/test_11-22.jpg', 'HI', 'Federation', NULL, NULL, '332322', 'eee', NULL, NULL, '23e', NULL, '5Fn7m5nvJEMLm6439W01GGYOKWLDQqYLi64IoxpL', 0, NULL, NULL, '$2y$10$9UV0t7rSPuLmEnQMIL6r/uyNmAXLqGJ.pzNX3X7R8hob1GnwOxKA.', '2018-12-21 10:56:44', NULL, NULL, NULL, NULL, '2018-12-21 05:26:44', '2019-01-24 05:31:22'),
(12, 'Francesco', 'Deo', 'En', NULL, 'saasas', 'Private Consumer', 'Francesco', 'b2c@event.com', '23458945', NULL, NULL, NULL, NULL, NULL, 'dqg06CppFRrQYarD2yKAmcsK1cASzl9dq5Uqm6Wv', 0, NULL, NULL, '$2y$10$9UV0t7rSPuLmEnQMIL6r/uyNmAXLqGJ.pzNX3X7R8hob1GnwOxKA.', '2018-12-21 10:58:24', NULL, '0000-00-00 00:00:00', NULL, 1, '2018-12-21 05:28:24', '2019-02-13 06:22:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`about_us_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `admin_admin_email_unique` (`email`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `event_event_category_id_foreign` (`event_category_id`);

--
-- Indexes for table `event_booking`
--
ALTER TABLE `event_booking`
  ADD PRIMARY KEY (`event_booking_id`);

--
-- Indexes for table `event_category`
--
ALTER TABLE `event_category`
  ADD PRIMARY KEY (`event_category_id`);

--
-- Indexes for table `event_ticket_assign`
--
ALTER TABLE `event_ticket_assign`
  ADD PRIMARY KEY (`event_ticket_assign_id`),
  ADD KEY `event_ticket_assign_event_id_foreign` (`event_id`);

--
-- Indexes for table `event_ticket_types`
--
ALTER TABLE `event_ticket_types`
  ADD PRIMARY KEY (`event_ticket_types_id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`faq_id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`hotel_id`);

--
-- Indexes for table `hotel_booking`
--
ALTER TABLE `hotel_booking`
  ADD PRIMARY KEY (`hotel_booking_id`);

--
-- Indexes for table `hotel_room`
--
ALTER TABLE `hotel_room`
  ADD PRIMARY KEY (`hotel_room_assign_id`),
  ADD KEY `hotel_room_assign_hotel_id_foreign` (`hotel_id`),
  ADD KEY `hotel_room_assign_hotel_room_type_id_foreign` (`hotel_room_type_id`);

--
-- Indexes for table `hotel_room_type`
--
ALTER TABLE `hotel_room_type`
  ADD PRIMARY KEY (`hotel_room_type_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meal`
--
ALTER TABLE `meal`
  ADD PRIMARY KEY (`meal_id`),
  ADD KEY `meal_hotel_id_foreign` (`hotel_id`);

--
-- Indexes for table `meal_type`
--
ALTER TABLE `meal_type`
  ADD PRIMARY KEY (`meal_type_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mm_res`
--
ALTER TABLE `mm_res`
  ADD PRIMARY KEY (`mm_res_id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `news_category_id_foreign` (`category_id`);

--
-- Indexes for table `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`package_id`);

--
-- Indexes for table `package_booking`
--
ALTER TABLE `package_booking`
  ADD PRIMARY KEY (`package_booking_id`);

--
-- Indexes for table `package_details`
--
ALTER TABLE `package_details`
  ADD PRIMARY KEY (`package_details_id`),
  ADD KEY `package_details_package_id_foreign` (`package_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `placeholder`
--
ALTER TABLE `placeholder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`testimonials_id`);

--
-- Indexes for table `transportation`
--
ALTER TABLE `transportation`
  ADD PRIMARY KEY (`transportation_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_user_username_unique` (`user_username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_contact_no_unique` (`contact_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `about_us_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `event_booking`
--
ALTER TABLE `event_booking`
  MODIFY `event_booking_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_category`
--
ALTER TABLE `event_category`
  MODIFY `event_category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `event_ticket_assign`
--
ALTER TABLE `event_ticket_assign`
  MODIFY `event_ticket_assign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_ticket_types`
--
ALTER TABLE `event_ticket_types`
  MODIFY `event_ticket_types_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `faq_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `hotel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `hotel_booking`
--
ALTER TABLE `hotel_booking`
  MODIFY `hotel_booking_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_room`
--
ALTER TABLE `hotel_room`
  MODIFY `hotel_room_assign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `hotel_room_type`
--
ALTER TABLE `hotel_room_type`
  MODIFY `hotel_room_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `meal`
--
ALTER TABLE `meal`
  MODIFY `meal_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `meal_type`
--
ALTER TABLE `meal_type`
  MODIFY `meal_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `mm_res`
--
ALTER TABLE `mm_res`
  MODIFY `mm_res_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `news_category`
--
ALTER TABLE `news_category`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `notification_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `package_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `package_booking`
--
ALTER TABLE `package_booking`
  MODIFY `package_booking_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `package_details`
--
ALTER TABLE `package_details`
  MODIFY `package_details_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `placeholder`
--
ALTER TABLE `placeholder`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `testimonials_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `transportation`
--
ALTER TABLE `transportation`
  MODIFY `transportation_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_event_category_id_foreign` FOREIGN KEY (`event_category_id`) REFERENCES `event_category` (`event_category_id`) ON DELETE CASCADE;

--
-- Constraints for table `event_ticket_assign`
--
ALTER TABLE `event_ticket_assign`
  ADD CONSTRAINT `event_ticket_assign_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`) ON DELETE CASCADE;

--
-- Constraints for table `hotel_room`
--
ALTER TABLE `hotel_room`
  ADD CONSTRAINT `hotel_room_assign_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`hotel_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `hotel_room_assign_hotel_room_type_id_foreign` FOREIGN KEY (`hotel_room_type_id`) REFERENCES `hotel_room_type` (`hotel_room_type_id`) ON DELETE CASCADE;

--
-- Constraints for table `meal`
--
ALTER TABLE `meal`
  ADD CONSTRAINT `meal_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`hotel_id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `news_category` (`category_id`) ON DELETE CASCADE;

--
-- Constraints for table `package_details`
--
ALTER TABLE `package_details`
  ADD CONSTRAINT `package_details_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `package` (`package_id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
